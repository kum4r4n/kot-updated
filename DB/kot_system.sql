-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 08:45 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kot_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discountRate` double NOT NULL,
  `TableID` varchar(50) NOT NULL,
  `discountid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discountRate`, `TableID`, `discountid`) VALUES
(20, 'Table:01', 1),
(50, 'Table:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `foodlist`
--

CREATE TABLE `foodlist` (
  `fid` varchar(20) NOT NULL,
  `foodName` varchar(50) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` double NOT NULL,
  `catergories` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foodlist`
--

INSERT INTO `foodlist` (`fid`, `foodName`, `stock`, `price`, `catergories`) VALUES
('001', 'Rice', 40, 100, 'dish'),
('002', 'mutton', 45, 200, 'dish');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoiceid` int(11) NOT NULL,
  `TableID` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `dish` int(11) NOT NULL,
  `drinks` int(11) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoiceid`, `TableID`, `date`, `time`, `dish`, `drinks`, `total`) VALUES
(100000001, 'Table:01', '2019/01/20', '01:14 PM', 0, 0, 500),
(100000002, 'Table:01', '2019/01/20', '01:18 PM', 1, 0, 500),
(100000003, 'Table:01', '2019/01/20', '01:24 PM', 1, 0, 500),
(100000004, 'Table:01', '2019/01/20', '05:17 PM', 1, 0, 500),
(100000005, 'Table:01', '2019/01/20', '05:19 PM', 1, 0, 500);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `orderid` int(11) NOT NULL,
  `fid` varchar(50) NOT NULL,
  `fName` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL,
  `catergories` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`orderid`, `fid`, `fName`, `price`, `qty`, `catergories`) VALUES
(1, '001', 'Rice', 500, 5, 'dish'),
(2, '002', 'mutton', 1000, 5, 'dish');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `foodlist`
--
ALTER TABLE `foodlist`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoiceid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
