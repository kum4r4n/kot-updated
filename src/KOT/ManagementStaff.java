package KOT;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.text.Highlighter.Highlight;

import Database.DBConnection;
import TableView.DBTablePrinter;

public class ManagementStaff {
	
	
	private double lowPrice = 5000.0;
	private double lowRate = 0.02;
	
	private double highPrice = 20000.0;
	private double highRate = 0.07;
	
	private double middlePrice = 10000.0;
	private double middleRate = 0.05;
	

	

	private String Table = "Select";

	public String getTable() {
		return Table;
	}

	public void setTable(String table) {
		Table = table;
	}

	private String tb;

	private double discountRate = 0.0;
	private int did;

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	public int getInvoice() {

		int invoice = 0;

		try {

			PreparedStatement ps;
			ResultSet rs;

			Connection conn = DBConnection.getConnection();

			ps = (PreparedStatement) conn.prepareStatement("SELECT MAX(invoiceid) AS id FROM invoice");
			rs = ps.executeQuery();

			while (rs.next()) {

				if (rs.getInt("id") == 0) {

					invoice = 100000001;

				} else {

					invoice = rs.getInt("id") + 1;
				}

			}

		} catch (SQLException er) {

			System.err.println("error  invoice" + er);

		}
		return invoice;

	}

	public void showInvoice() {

		Connection conn = null;

		try {

			conn = DBConnection.getConnection();

			System.out.println("-| Hi Sir ! This is the list of Invoices ! |- \n");

			DBTablePrinter.printTable(conn, "invoice");

			System.out.println("");
			restart();

		} catch (Exception e) {

		}

		finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}

		}

	}

	public void restart() {

		Scanner restart = new Scanner(System.in);

		System.out.println("");
		System.out.println(
				"-| Hi Sir ! Are You Want to Another Process ? \n\t1 - yes \n\t2 - Go Main Menu \n\t0 - Close");
		int re = restart.nextInt();

		if (re == 1) {

			start();

		} else if (re == 2) {

			Person person = new Person();
			person.WhoAreYou();

		} else if (re == 0) {

			System.exit(0);

		}

	}

	public void start() {

		Scanner manage = new Scanner(System.in);

		System.out.println(
				"-| Hi Sir ! What you want |- \n\t1 - Invoice Detials \n\t2 - Meanage Customer Bill Statement \n\t3 - Main Menu \n\t0 - Close");

		int order = manage.nextInt();

		if (order == 1) {

			showInvoice();

		} else if (order == 2) {

			settingDicount();

		} else if (order == 3) {

			Person person = new Person();
			person.WhoAreYou();

		} else if (order == 0) {

			System.exit(0);

		}

	}

	public void maxid() {

		PreparedStatement psmaxid = null;
		ResultSet rs10 = null;
		try {

			Connection connmaxid = DBConnection.getConnection();

			psmaxid = (PreparedStatement) connmaxid
					.prepareStatement("SELECT MAX(discountId) AS id FROM kot_system.`Discount`");

			rs10 = psmaxid.executeQuery();

			while (rs10.next()) {

				setDid(rs10.getInt("id"));

				connmaxid.close();

			}

		} catch (SQLException er) {

		}

		finally {

			try {
				rs10.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}
	
	public double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public double getLowRate() {
		return lowRate;
	}

	public void setLowRate(double lowRate) {
		this.lowRate = lowRate/100;
	}

	public double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(double highPrice) {
		this.highPrice = highPrice;
	}

	public double getHighRate() {
		return highRate;
	}

	public void setHighRate(double highRate) {
		this.highRate = highRate/100;
	}

	public double getMiddlePrice() {
		return middlePrice;
	}

	public void setMiddlePrice(double middlePrice) {
		this.middlePrice = middlePrice;
	}

	public double getMiddleRate() {
		return middleRate;
	}

	public void setMiddleRate(double middleRate) {
		this.middleRate = middleRate/100;
	}
	
	

	public void settingDicount() {
		
		Scanner get = new Scanner(System.in);
		System.out.println("-| Welcome to making Discount Process");
		
		System.out.println("-| which Discount you want you change \n\t1 - low \n\t2 - middle \n\t3 - High \n\t0 - Main Menu|-");
		
		int getNumber = get.nextInt();
		
		
		if(getNumber == 1) {
			
			
			System.out.println("type your low discount price ");
			
			setLowPrice(get.nextDouble());
			
			System.out.println("type discount rate for "+getLowPrice());
			
			setLowRate(get.nextDouble());
			
			System.out.println("it will be updated!");
			
		}else if(getNumber == 2) {
			
			
			System.out.println("type your middle discount price ");
			
			setMiddlePrice(get.nextDouble());
			
			System.out.println("type discount rate for "+getMiddlePrice());
			
			setMiddleRate(get.nextDouble());
			
			System.out.println("it will be updated!");
			
			
			
		}else if(getNumber == 3) {
			
			
			System.out.println("type your high discount price ");
			
			setHighPrice(get.nextDouble());
			
			System.out.println("type discount rate for "+getHighPrice());
			
			setHighRate(get.nextDouble());
			
			System.out.println("it will be updated!");
			
			
		}else if(getNumber == 0) {
			
			start();
			
			
			
			
		} else {
			
			System.err.println("----------------------------------------------");
			System.err.println("|XXXXX| Please Select Correct Option ! |XXXXX|");
			System.err.println("----------------------------------------------");

			settingDicount();

			
			
			
		}
		
		
		
		
		
		
		

	}

	public double makeDiscount(double total) {

		double DiscountAmount = 0.0;

		if (total > getHighPrice()) {

			return DiscountAmount = total * getHighRate();

		} else if (total >= getMiddlePrice()) {

			return DiscountAmount = total * getMiddleRate();

		} else if (total >= getLowPrice()) {

			return DiscountAmount = total * getLowRate();

		} else {

			return DiscountAmount = 0.0;

		}

	}

}
