package KOT;

import java.util.Scanner;

public class Person {

	Scanner gru = new Scanner(System.in);

	public void WhoAreYou() {
		
		Waiter waiter = new Waiter();
		waiter.resetOrder();
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		System.out.println("-| Hi Sir..!  Who are You ? |-");
		System.out.println("\t1 - Waiter \n\t2 - KitchenStaff \n\t3 - ManagementStaff \n\t0 - Close");
		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		int who = gru.nextInt();

		if (who == 1) {

			Waiter();

		} else if (who == 2) {

			KitchenStaff();

		} else if (who == 3) {

			ManagementStaff();

		} else if (who == 0) {

			System.exit(0);

		} else {

			System.err.println("----------------------------------------------");
			System.err.println("|XXXXX| Please Select Correct Option ! |XXXXX|");
			System.err.println("----------------------------------------------");

			WhoAreYou();

		}

	}

	public void Waiter() {

		Waiter waiter = new Waiter();
		waiter.start();

	}

	public void KitchenStaff() {

	}

	public void ManagementStaff() {
		
		ManagementStaff staff = new ManagementStaff();
		staff.start();
		

	}

}
