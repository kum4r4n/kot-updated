package KOT;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import java.sql.PreparedStatement;

import Database.DBConnection;
import TableView.DBTablePrinter;

public class Waiter {

	public int oid;
	private int countdish;
	private Double DiscountAmount = 0.0;

	private int countdrinks;

	private String tb;

	private Double OrderTotal;

	Scanner user = new Scanner(System.in);

	public void ShowFoodItems() {

		Connection conn = null;

		try {

			resetOrder();

			Table table = new Table();

			table.orderTable();

			tb = table.getTable();

			conn = DBConnection.getConnection();

			System.out.println("-| Hi Sir Select Foods From this List ! |- \n");

			DBTablePrinter.printTable(conn, "foodlist");

		} catch (Exception e) {

		}

		finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}

		}

	}

	public void Order() {

		Scanner ofood = new Scanner(System.in);

		System.out.print("-| Food ID :- ");
		String id = ofood.nextLine();

		ResultSet rsc;

		PreparedStatement psc;

		Connection connc = null;

		try {
			connc = DBConnection.getConnection();

			String query = "Select * from foodlist where fid = ?";

			psc = (PreparedStatement) connc.prepareStatement(query);

			psc.setString(1, id);

			rsc = psc.executeQuery();

			if (!rsc.next()) {

				System.err.println(
						"----------------------------------------------\n|XXXXX| Please Select Correct Option ! |XXXXX|\n----------------------------------------------");

				System.out.println("-| Please Select Available Food Items ! |-");

				Order();

			} else {

				System.out.print("-| How Many You Want From Selected Food Item " + id + " :- ");
				int oqty = ofood.nextInt();

				ResultSet rs;

				PreparedStatement ps;
				PreparedStatement pso;
				Connection conn = null;

				try {
					conn = DBConnection.getConnection();

					// String query = "Select * from foodlist where fid = ?";

					ps = (PreparedStatement) conn.prepareStatement(query);

					ps.setString(1, id);

					rs = ps.executeQuery();

					while (rs.next()) {

						String OrderFoodId = rs.getString("fid");
						String OrderFoodName = rs.getString("foodName");
						int OrderStock = rs.getInt("stock");
						Double OrderPrice = rs.getDouble("price");
						String Catergories = rs.getString("catergories");

						String Order = "INSERT INTO kot_system.`order` (orderid,fid, fName, price,qty,catergories) VALUES (?, ?,?, ?,?,?)";

						pso = (PreparedStatement) conn.prepareStatement(Order);
						maxid();
						pso.setInt(1, getOid() + 1);
						pso.setString(2, OrderFoodId);
						pso.setString(3, OrderFoodName);
						pso.setDouble(4, (oqty * OrderPrice));
						pso.setInt(5, oqty);
						pso.setString(6, Catergories);

						pso.executeUpdate();

						Connection connset = DBConnection.getConnection();

						String ustock = "Update foodlist set stock = ? where fid = ? ";
						PreparedStatement psupdate = (PreparedStatement) connset.prepareStatement(ustock);
						psupdate.setInt(1, OrderStock - oqty);
						psupdate.setString(2, id);

						psupdate.executeUpdate();

						connset.close();

					}

				} catch (SQLException e) {

					e.printStackTrace();
				}

				finally {

					try {
						conn.close();
					} catch (SQLException e) {

						e.printStackTrace();
					}

				}

			}

		} catch (SQLException e) {

		}

	}

	public void ShowOrderTable() {

		Connection conn = null;

		try {

			conn = DBConnection.getConnection();

			DBTablePrinter.printTable(conn, "kot_system.`order`");

		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {

			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public void maxid() {

		PreparedStatement psmaxid = null;
		ResultSet rs10 = null;
		try {

			Connection connmaxid = DBConnection.getConnection();

			psmaxid = (PreparedStatement) connmaxid
					.prepareStatement("SELECT MAX(orderid) AS id FROM kot_system.`order`");

			rs10 = psmaxid.executeQuery();

			while (rs10.next()) {

				setOid(rs10.getInt("id"));

				connmaxid.close();

			}

		} catch (SQLException er) {

		}

		finally {

			try {
				rs10.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}

	public double getDiscountAmount() {

		ManagementStaff mStaf = new ManagementStaff();
		
		return mStaf.makeDiscount(OrderTotal);
		
	}
	
	
	
	

	public void totalPrice() {

		try {

			ResultSet rs3;

			Connection connn = DBConnection.getConnection();

			String uqty = "select sum(price) as total from kot_system.order";

			PreparedStatement ps5;

			ps5 = (PreparedStatement) connn.prepareStatement(uqty);

			rs3 = ps5.executeQuery();

			while (rs3.next()) {

				OrderTotal = rs3.getDouble("total");

				ps5.executeQuery();

			}

		} catch (SQLException e) {

			// e.printStackTrace();

		}
	}

	public void getOrder() {

		Order();
		ShowOrderTable();
		System.out.println("-| Are You Want to Buy Another Food Item ? |- \n\t1 - yes \n\t2 - No \n\t0 - close");

		int order = user.nextInt();

		if (order == 1) {

			getOrder();

		} else if (order == 2) {

			totalPrice();
			billPrint();
			confimOrder();

		} else if (order == 0) {

			System.exit(0);

		}

	}

	public void billPrint() {

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		System.out.println("-| Your Bill :- " + OrderTotal + " |-");

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		
		

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		System.out.println("-| Your Discounted  is :- " + getDiscountAmount() + " |-");

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");

		Double DiscountPrice = OrderTotal - getDiscountAmount();

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");
		System.out.println("-| Your Total Bill  is :- " + DiscountPrice + " |-");

		System.out.println(
				"-----------------------------------------------------------------------------------------------------");

	}

	public void confimOrder() {

		System.out.println(
				"-| Are You Conform the Order Sir !  \n\t1 - yes \n\t2 - Are You Customiz the Order ?  \n\t0 - Close ");

		int conform = user.nextInt();

		if (conform == 1) {

			ManagementStaff ms = new ManagementStaff();

			sendInvoice();
			sendKitchen();

			resetOrder();
			afterOrder();

		} else if (conform == 2) {

			Scanner delete = new Scanner(System.in);

			System.out.println("-| Please Type the OrderId to Delete Unwanted Order Item ! |-");

			int orderid = delete.nextInt();

			customOrder(orderid);

			ShowOrderTable();
			totalPrice();
			billPrint();
			confimOrder();

		} else if (conform == 0) {

			System.exit(0);

		} else {

			System.err.println("----------------------------------------------");
			System.err.println("|XXXXX| Please Select Correct Option ! |XXXXX|");
			System.err.println("----------------------------------------------");

			confimOrder();

		}

	}

	public void customOrder(int FoodID) {

		String sqldelete = "DELETE  FROM kot_system.`order` where orderid = " + FoodID;

		Connection conn8;

		try {
			conn8 = DBConnection.getConnection();

			PreparedStatement stmt = (PreparedStatement) conn8.prepareStatement(sqldelete);

			stmt.execute();

		} catch (SQLException e) {

			// e.printStackTrace();
		}

	}

	public void afterOrder() {

		Scanner afor = new Scanner(System.in);

		System.out.println("-| Are You want To make another Order ? \n\t1 - Yes \n\t0 - No ");

		int conform = afor.nextInt();

		if (conform == 1) {

			start();

		} else if (conform == 0) {

			System.out.println(
					"-----------------------------------------------------------------------------------------------------");
			System.out.println(
					"------------------------------------| Thanks For Using this |----------------------------------------");

			System.out.println(
					"-----------------------------------------------------------------------------------------------------");

			System.exit(0);

		} else {

			System.err.println("----------------------------------------------");
			System.err.println("|XXXXX| Please Select Correct Option ! |XXXXX|");
			System.err.println("----------------------------------------------");

		}

	}

	public void resetOrder() {

		String sqldelete = "DELETE  FROM kot_system.`order`";

		Connection conn8;

		try {
			conn8 = DBConnection.getConnection();

			PreparedStatement stmt = (PreparedStatement) conn8.prepareStatement(sqldelete);

			stmt.execute();

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public int getCountdish() {
		return countdish;
	}

	public void setCountdish(int countdish) {
		this.countdish = countdish;
	}

	public int getCountdrinks() {
		return countdrinks;
	}

	public void setCountdrinks(int countdrinks) {
		this.countdrinks = countdrinks;
	}

	public void getcount() {

		try {

			ResultSet rs3;

			Connection connn = DBConnection.getConnection();

			String uqty = "select (select count(catergories) from kot_system.order where catergories= ? ) "
					+ "as countdish,(select count(catergories) from kot_system.order where catergories= ?) "
					+ "as countdrinks;";

			PreparedStatement ps5;

			ps5 = (PreparedStatement) connn.prepareStatement(uqty);

			ps5.setString(1, "dish");
			ps5.setString(2, "drinks");

			rs3 = ps5.executeQuery();

			while (rs3.next()) {

				setCountdish(rs3.getInt("countdish"));
				setCountdrinks(rs3.getInt("countdrinks"));

				ps5.executeQuery();

			}

		} catch (SQLException e) {

			// e.printStackTrace();
		}

	}

	public String getdate() {

		String date = new SimpleDateFormat("YYYY/MM/dd").format(Calendar.getInstance().getTime());

		return date;

	}

	public String gettime() {

		String time = new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());

		return time;

	}

	public void sendInvoice() {

		getcount();

		ManagementStaff mstaff = new ManagementStaff();

		String sqlinsert = "INSERT INTO kot_system.`invoice` (invoiceid,TableID,date, time, dish,drinks, total) VALUES (?,?, ?,?,?,?,?)";

		try {

			Table table = new Table();

			Connection conn = DBConnection.getConnection();

			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sqlinsert);

			ps.setInt(1, mstaff.getInvoice());
			ps.setString(2, tb);
			ps.setString(3, getdate());
			ps.setString(4, gettime());
			ps.setInt(5, getCountdish());
			ps.setInt(6, getCountdrinks());
			ps.setDouble(7, OrderTotal);
			ps.executeUpdate();

			conn.close();

		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

	public void sendKitchen() {

		
		
		
		
	}

	public void start() {
		ShowFoodItems();
		getOrder();

	}

}
